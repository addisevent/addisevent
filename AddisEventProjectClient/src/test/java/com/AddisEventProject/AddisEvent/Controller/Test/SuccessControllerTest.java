package com.AddisEventProject.AddisEvent.Controller.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

public class SuccessControllerTest {
	@Autowired
	private MockMvc mockMvc;
	@Test
	public void testSuccessEventPage()throws Exception{
		mockMvc.perform(get("/success"))
		.andExpect(status().isOk())
				.andExpect(view().name("success"));
}
}
