package com.AddisEventProject.AddisEvent.Controller.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.AddisEventGroup.AddisEvent.controllers.LogInController;

@RunWith(SpringRunner.class)
@WebMvcTest(LogInController.class)
public class LoginControllerTest {
	@Autowired
	private MockMvc mockMvc;
	@Test
	public void testLoginPage()throws Exception{
		mockMvc.perform(get("/"))
		.andExpect(status().isOk())
				.andExpect(view().name("login"));
				
	}
}
