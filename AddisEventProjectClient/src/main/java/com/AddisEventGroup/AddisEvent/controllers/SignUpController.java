package com.AddisEventGroup.AddisEvent.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.aspectj.*;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import org.apache.catalina.core.ApplicationContext;

import org.springframework.beans.factory.annotation.Autowired;
import com.AddisEventGroup.AddisEvent.security.user;
import com.AddisEventGroup.AddisEvent.services.UserService;
import com.AddisEventGroup.AddisEvent.repositories.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/SignUp")
public class SignUpController {
	
	//ConfigurableApplicationContext ctx=new AnnotationConfigApplicationContext("com.example.demo.data");
	
	
	@Autowired
    private UserService userService;
	
	@ModelAttribute(name="user")
	public user theuser() {
		return new user();
	}
	private UserRepository userRepo;
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	//@Autowired
	public SignUpController(UserRepository userRepo,BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepo=userRepo;
		this.bCryptPasswordEncoder =bCryptPasswordEncoder;
	}
	@GetMapping
	public String ShowSignUpForm() {
		return "SignUp";
	}
	@PostMapping
	public String createNewUser(@Valid user theuser, BindingResult bindingResult, Model model) {
        
		
        if (bindingResult.hasErrors()) {
        	
        	
        	return "SignUp";
        } else {
        	
            userService.saveUser(theuser);
            
            model.addAttribute("successMessage", "User has been registered successfully");
            
            return "success";
        }
		}
    
	
	@GetMapping("/access-denied")
    public String accessDenied(){
        return "access_denied";
    }
	/*public String ProcessSignUp(RegistrationForm form) {
		
		user savedUser=userRepo.save(form.toUser(bCryptPasswordEncoder));
		//ctx.close();
		log.info("New User Signed Up!: " + savedUser);
		userRepo.findAll().forEach(i -> log.info(i.getUsername()));
		userRepo.findAll().forEach(i -> log.info(i.getPassword()));
		return "redirect:/login";
	}*/
}
