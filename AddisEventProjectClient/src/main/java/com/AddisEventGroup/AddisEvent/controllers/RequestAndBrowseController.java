package com.AddisEventGroup.AddisEvent.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/reqAndBrowse")
public class RequestAndBrowseController {
	
	@GetMapping
	public String showreq() {
		return("requestandbrowse");
	}
}
