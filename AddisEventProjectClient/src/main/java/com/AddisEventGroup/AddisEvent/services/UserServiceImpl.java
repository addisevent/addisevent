package com.AddisEventGroup.AddisEvent.services;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.AddisEventGroup.AddisEvent.repositories.RoleRepository;
import com.AddisEventGroup.AddisEvent.repositories.UserRepository;
import com.AddisEventGroup.AddisEvent.security.Role;
import com.AddisEventGroup.AddisEvent.security.user;

@Service
public class UserServiceImpl implements UserService{
	private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                       RoleRepository roleRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }
    
    public user findUserByUsername(String username) {
    	return userRepository.findByUsername(username);
    }
    Role userRole;
    public void saveUser(user theuser) {
        theuser.setPassword(bCryptPasswordEncoder.encode(theuser.getPassword()));
        theuser.setEnabled(1);
        if(Objects.equals(theuser.getTherole(), new String("EventUser"))) {
        userRole = roleRepository.findByRole("EventUser");
        }
        else {
        	 userRole = roleRepository.findByRole("EventOrganizer");
        }
        theuser.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        userRepository.save(theuser);
    }
    
    @Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		user theuser = userRepository.findByUsername(username);
		
		if(theuser != null) {
			return theuser;
		}
		throw new UsernameNotFoundException("User '" + username + "' not found");
	}

}
