package com.AddisEventGroup.AddisEvent.services;

import javax.validation.Valid;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.AddisEventGroup.AddisEvent.security.user;

public interface UserService extends UserDetailsService{
	//user findUserByUsername(String username);

	//void saveUser(@Valid user theuser);

	user findUserByUsername(String username);

	void saveUser(@Valid user theuser);
}
