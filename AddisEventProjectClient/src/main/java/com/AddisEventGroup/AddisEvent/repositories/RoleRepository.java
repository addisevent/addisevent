package com.AddisEventGroup.AddisEvent.repositories;

import org.springframework.data.repository.CrudRepository;

import com.AddisEventGroup.AddisEvent.security.Role;

public interface RoleRepository extends CrudRepository <Role,Long>{
    

	Role findByRole(String role);
}
