package com.AddisEventGroup.AddisEvent.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.AddisEventGroup.AddisEvent.domains.RequestForm;
import com.AddisEventGroup.AddisEvent.security.Role;

public interface FormRepository extends CrudRepository <RequestForm,Long>{
    
List<RequestForm> findByStatus(String status);
RequestForm findByEventname(String eventname);
	
}