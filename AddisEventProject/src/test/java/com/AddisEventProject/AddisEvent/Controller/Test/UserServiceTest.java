package com.AddisEventProject.AddisEvent.Service.Test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.AddisEventGroup.AddisEvent.repositories.UserRepository;
import com.AddisEventGroup.AddisEvent.security.User;

import com.AddisEventGroup.AddisEvent.services.UserService;
import com.AddisEventGroup.AddisEvent.services.UserServiceImpl;


@RunWith(SpringRunner.class)
public class UserServiceTest {
	@TestConfiguration
	static class UserServiceImplTestContextConfiguration {
		@Bean
		public UserService userService() {
			return new UserServiceImpl(null, null, null);
		}
		@Autowired
	    private UserService userService;
		
		@MockBean
		private static UserRepository userRepository;	
	    
		@Before
		public void setUp() {
		    User user=new User();
		    user.setUsername("user");
		    
		    Mockito.when(userRepository.findById(1l))
		      .thenReturn(user);
		}
		
	    @Test
	    public void whenValidName_thenEmployeeShouldBeFound() {
	        User found = userService.findById(1l);
	      
	         assertThat(found.getUsername())
	          .isEqualTo("user");
	     }
	}}
