package com.AddisEventGroup.AddisEvent.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.AddisEventGroup.AddisEvent.domains.RequestForm;
import com.AddisEventGroup.AddisEvent.repositories.FormRepository;
import com.AddisEventGroup.AddisEvent.security.user;

@Controller
@RequestMapping("/reqForm")
public class reqFormController {
	
	@Autowired
	FormRepository formRepo;
	
	@GetMapping
		public String showtheForm() {
			return ("formview");
		}
		
		
		@PostMapping
		public String createNewForm(@Valid RequestForm theForm, BindingResult bindingResult, Model model) {
	        if (bindingResult.hasErrors()) {
	        	
	        	
	        	return "RequestPage";
	        } else {
	        	
	            formRepo.save(theForm);
	            
	            //model.addAttribute("successMessage", "User has been registered successfully");
	            
	            return "success";
	        }
		}
}
