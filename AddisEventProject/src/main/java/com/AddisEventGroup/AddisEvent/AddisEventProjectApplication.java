package com.AddisEventGroup.AddisEvent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AddisEventProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AddisEventProjectApplication.class, args);
	}
}
