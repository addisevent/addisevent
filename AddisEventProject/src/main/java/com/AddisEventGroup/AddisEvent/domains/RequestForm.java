package com.AddisEventGroup.AddisEvent.domains;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.AddisEventGroup.AddisEvent.security.Role;
import com.AddisEventGroup.AddisEvent.security.user;

import lombok.Data;

@Data
@Entity

public class RequestForm {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank(message="Please Enter an EventName")
	private  String eventname;
	
	@NotBlank(message="Please Enter an Event Description")
	private  String description;
	
	@NotBlank(message="Please Enter an Event Category")
	private String category;
	
	@NotBlank(message="Please Enter an Event Place")
	private String eventplace;
	
	@NotBlank(message="Please Enter an Event Date")
	private String eventdate;
	/*@Lob
    @Column(name="image")
    private byte[] pic;*/
	
	
	private String status;
	
	@PrePersist
	void status() {
		this.status="Not Approved";
	}
	
	
	
	
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinTable(name="RequestedEvent",joinColumns= {@JoinColumn(name="form_id")},inverseJoinColumns= {@JoinColumn(name="user_id")})
	private user userr;
}
