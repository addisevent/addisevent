package com.AddisEventGroup.AddisEvent.repositories;

import org.springframework.data.repository.CrudRepository;

import com.AddisEventGroup.AddisEvent.domains.RequestForm;
import com.AddisEventGroup.AddisEvent.security.Role;

public interface FormRepository extends CrudRepository <RequestForm,Long>{
    

	
}