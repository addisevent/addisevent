package com.AddisEventGroup.AddisEvent.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.AddisEventGroup.AddisEvent.domains.RequestForm;
import com.AddisEventGroup.AddisEvent.repositories.FormRepository;

@Service
public class FormServiceImpl implements FormService{
	
	FormRepository formRepo;
	
	@Autowired
	public FormServiceImpl(FormRepository formRepo) {
		this.formRepo=formRepo;
	}
	@Override
	public RequestForm save(RequestForm requestForm) {
		// TODO Auto-generated method stub
		return formRepo.save(requestForm);
	}

	@Override
	public Optional<RequestForm> findById(Long id) {
		// TODO Auto-generated method stub
		return formRepo.findById(id);
	}

	@Override
	public Iterable<RequestForm> findAll() {
		// TODO Auto-generated method stub
		return formRepo.findAll();
	}
	@Override
	public List<RequestForm> findByStatus(String status) {
		// TODO Auto-generated method stub
		return formRepo.findByStatus(status);
	}
	@Override
	public RequestForm findByEventname(String eventname) {
		// TODO Auto-generated method stub
		return formRepo.findByEventname(eventname);
	}
	

}
