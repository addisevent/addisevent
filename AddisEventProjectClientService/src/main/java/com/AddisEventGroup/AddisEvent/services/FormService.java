package com.AddisEventGroup.AddisEvent.services;

import java.util.List;
import java.util.Optional;

import com.AddisEventGroup.AddisEvent.domains.RequestForm;



public interface FormService {
	
	public RequestForm save(RequestForm requestForm);
	Optional<RequestForm> findById(Long id);
	Iterable<RequestForm> findAll();
	public List<RequestForm> findByStatus(String status);
	public RequestForm findByEventname(String eventname);
}
