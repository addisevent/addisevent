package com.AddisEventGroup.AddisEvent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class AddisEventProjectClientApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(AddisEventProjectClientApplication.class, args);
	}
	@Override
	 protected SpringApplicationBuilder configure(
	  SpringApplicationBuilder builder) {
	 return builder.sources(AddisEventProjectClientApplication.class);
	 }
}
