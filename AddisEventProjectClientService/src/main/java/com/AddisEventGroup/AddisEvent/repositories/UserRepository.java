package com.AddisEventGroup.AddisEvent.repositories;



import org.springframework.data.repository.CrudRepository;

import com.AddisEventGroup.AddisEvent.security.user;

public interface UserRepository extends CrudRepository <user,String>{
	

	user findByUsername(String username);

	
}
