package com.AddisEventGroup.AddisEvent.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.AddisEventGroup.AddisEvent.domains.RequestForm;
import com.AddisEventGroup.AddisEvent.domains.RequestForm.Type;
import com.AddisEventGroup.AddisEvent.repositories.FormRepository;
import com.AddisEventGroup.AddisEvent.services.FormService;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Controller

public class SecondHomeController {
	private FormService formService;
	
	@Autowired
	public SecondHomeController(FormService formService) {
		this.formService=formService;
	}
	
	@ModelAttribute
	public void addEventToModel(Model models) {
		List<RequestForm> event = new ArrayList<>();
		formService.findByStatus("Approved")
				.forEach(i->event.add(i));
		Type[] typess = RequestForm.Type.values();
		
		for (Type type: typess) {
			models.addAttribute(type.toString().toLowerCase(), filterByType(event, type));
		}
	}
	
	private List<RequestForm> filterByType(List<RequestForm> event, Type type){
		return event
				.stream()
				.filter(x->x.getCategory().equals(type))
				.collect(Collectors.toList());
	}
	
	@GetMapping("/SecondHome")
	public String showSecondHome() {
		return "SecondHome";
	}
	
	@RequestMapping(value = {"/ratingSecond"}, method = RequestMethod.POST)
	public void postRatingg(RequestForm ratedEvent) {
		log.info("Rated event");
		
	}
	
}
