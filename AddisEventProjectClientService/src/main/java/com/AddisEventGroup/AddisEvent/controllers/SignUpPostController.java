package com.AddisEventGroup.AddisEvent.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.aspectj.*;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.AddisEventGroup.AddisEvent.repositories.UserRepository;
import com.AddisEventGroup.AddisEvent.security.user;
import com.AddisEventGroup.AddisEvent.services.UserService;

import javax.validation.Valid;

import org.apache.catalina.core.ApplicationContext;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/SignUpPost")
public class SignUpPostController {
	
	//ConfigurableApplicationContext ctx=new AnnotationConfigApplicationContext("com.example.demo.data");
	
	
	@Autowired
    private UserService userService;
	
	@ModelAttribute(name="user")
	public user theuser() {
		return new user();
	}
	private UserRepository userRepo;
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	//@Autowired
	public SignUpPostController(UserRepository userRepo,BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepo=userRepo;
		this.bCryptPasswordEncoder =bCryptPasswordEncoder;
	}
	
	@PostMapping
	public String createNewUser(@Valid user theuser, BindingResult bindingResult, Model model) {
        
		
        if (bindingResult.hasErrors()) {
        	
        	log.info("INSIDE THE BINDING ERRORS");
        	return "SignUp";
        } else {
        	
        	log.info("INSIDE THE SUCCESSFUL SIGNUP");
            userService.saveUser(theuser);
            
            model.addAttribute("successMessage", "User has been registered successfully");
            
            return "success";
        }
		}
    
	
	
	/*public String ProcessSignUp(RegistrationForm form) {
		
		user savedUser=userRepo.save(form.toUser(bCryptPasswordEncoder));
		//ctx.close();
		log.info("New User Signed Up!: " + savedUser);
		userRepo.findAll().forEach(i -> log.info(i.getUsername()));
		userRepo.findAll().forEach(i -> log.info(i.getPassword()));
		return "redirect:/login";
	}*/
}
