package com.AddisEventGroup.AddisEvent.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.AddisEventGroup.AddisEvent.domains.RequestForm;
import com.AddisEventGroup.AddisEvent.domains.RequestForm.Type;
import com.AddisEventGroup.AddisEvent.repositories.FormRepository;
import com.AddisEventGroup.AddisEvent.services.FormService;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.stream.Collectors;
@Slf4j
@Controller

public class AdminApproveController {
	private FormService formService;

	@Autowired
	public AdminApproveController(FormService formService) {
		this.formService=formService;
	}
	
	
	
	@ModelAttribute
	public void addEventToModel(Model model) {
		List<RequestForm> events = new ArrayList<>();
		formService.findByStatus("Not Approved")
							.forEach(i->events.add(i));
		
		Type[] types = RequestForm.Type.values();
		
		for (Type type: types) {
			model.addAttribute(type.toString().toLowerCase(), filterByType(events, type));
		}
	}
	
	private List<RequestForm> filterByType(List<RequestForm> events, Type type){
		return events
				.stream()
				.filter(x->x.getCategory().equals(type))
				.collect(Collectors.toList());
	}
	
	@GetMapping("/AdminApprove")
	public String showHomePage() {
		return("AdminApprove");
	}
	
	//@PostMapping("/approving")
	@RequestMapping(value = {"/approving"}, method = RequestMethod.POST)
	public String postApprove(RequestForm approvedEvent) {
		RequestForm found=formService.findByEventname(approvedEvent.getEventname());
		found.setStatus("Approved");
		formService.save(found);
		return "success";
		
	}
	
	@RequestMapping(value = {"/approvingg"}, method = RequestMethod.POST)
	public void postApprovee(RequestForm ratedEvent) {
		log.info("INCREASING");
		/*RequestForm found=formRepo.findByEventname(ratedEvent.getEventname());
		found.setRate(found.getRate()+1);
		log.info("INCREASED THE RATE!");
		formRepo.save(found);*/
		
		
	}
}
