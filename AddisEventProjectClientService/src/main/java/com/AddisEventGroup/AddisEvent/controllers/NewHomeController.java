package com.AddisEventGroup.AddisEvent.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.AddisEventGroup.AddisEvent.domains.RequestForm;
import com.AddisEventGroup.AddisEvent.domains.RequestForm.Type;
import com.AddisEventGroup.AddisEvent.repositories.FormRepository;
import com.AddisEventGroup.AddisEvent.services.FormService;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.stream.Collectors;
@Slf4j
@Controller

public class NewHomeController {
	private FormService formService;
	
	@Autowired
	public NewHomeController(FormService formService) {
		this.formService=formService;
	}
	
	
	
	@ModelAttribute
	public void addIngredientsToModel(Model model) {
		List<RequestForm> events = new ArrayList<>();
		formService.findAll()
							.forEach(i->events.add(i));
		
		Type[] types = RequestForm.Type.values();
		
		for (Type type: types) {
			model.addAttribute(type.toString().toLowerCase(), filterByType(events, type));
		}
	}
	
	private List<RequestForm> filterByType(List<RequestForm> events, Type type){
		return events
				.stream()
				.filter(x->x.getCategory().equals(type))
				.collect(Collectors.toList());
	}
	
	@GetMapping("/NewHome")
	public String showHomePage() {
		return("NewHome");
	}
	
	//@PostMapping("/approving")
	/*@RequestMapping(value = {"/approving"}, method = RequestMethod.POST)
	public String PostApprove(RequestForm approvedEvent) {
		RequestForm found=formRepo.findByEventname(approvedEvent.getEventname());
		found.setStatus("Approved");
		formRepo.save(found);
		return "success";
		
	}*/
}
