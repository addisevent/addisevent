package com.AddisEventGroup.AddisEvent.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

public class AccessDeniedController {
	
	@GetMapping("/403")
	public String accessDeniedShow() {
		return("access_denied");
	}
}
