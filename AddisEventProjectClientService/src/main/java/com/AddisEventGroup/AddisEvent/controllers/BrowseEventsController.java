package com.AddisEventGroup.AddisEvent.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/BrowseEvents")
public class BrowseEventsController {
	
	@GetMapping
	public String showBrowseEventsPage() {
		return "BrowseEventsPage";
	}
}






